package com.itcast.chongyang.dao;

import com.itcast.chongyang.domain.Product;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

public interface IProductDao {

    public List<Product> findAll() throws Exception;

    public Product findById(String id) throws Exception;

    @Insert("insert into product(productNum,productName,cityName,departureTime,productPrice,productDesc,productStatus) values(#{productNum},#{productName},#{cityName},#{departureTime},#{productPrice},#{productDesc},#{productStatus})")
    public void save(Product product) throws Exception;
}
