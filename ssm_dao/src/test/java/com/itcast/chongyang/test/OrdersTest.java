package com.itcast.chongyang.test;

import com.itcast.chongyang.dao.IOrdersDao;
import com.itcast.chongyang.dao.IProductDao;
import com.itcast.chongyang.domain.Orders;
import com.itcast.chongyang.domain.Product;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class OrdersTest {
    @Test
    public void testFindById() throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-dao.xml");
        IProductDao dao = ac.getBean(IProductDao.class);
        Product byId = dao.findById("676C5BD1D35E429A8C2E114939C5685A");
        System.out.println(byId);
    }

    @Test
    public void testFindOrders() throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-dao.xml");
        IOrdersDao ordersDao = ac.getBean(IOrdersDao.class);
        Orders orders = ordersDao.findById("0E7231DC797C486290E8713CA3C6ECCC");
        System.out.println(orders);
    }
}
