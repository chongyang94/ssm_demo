package com.itcast.chongyang.test;

import com.itcast.chongyang.dao.IProductDao;
import com.itcast.chongyang.domain.Product;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class ProductTest {
    @Test
    public void testFindAll() throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-dao.xml");
        IProductDao dao = ac.getBean(IProductDao.class);
        List<Product> products = dao.findAll();
        System.out.println(products);
    }


}
