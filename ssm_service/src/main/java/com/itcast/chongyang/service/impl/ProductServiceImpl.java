package com.itcast.chongyang.service.impl;

import com.itcast.chongyang.dao.IProductDao;
import com.itcast.chongyang.domain.Product;
import com.itcast.chongyang.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("/productService")
public class ProductServiceImpl implements ProductService {

    @Autowired
    public IProductDao productDao;
    @Override
    public List<Product> findAll() throws Exception {
        return productDao.findAll();
    }

    @Override
    public void save(Product product) throws Exception {
        productDao.save(product);
    }
}
