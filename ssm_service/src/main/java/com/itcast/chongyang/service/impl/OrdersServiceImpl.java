package com.itcast.chongyang.service.impl;

import com.github.pagehelper.PageHelper;
import com.itcast.chongyang.dao.IOrdersDao;
import com.itcast.chongyang.domain.Orders;
import com.itcast.chongyang.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("/ordersService")
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    public IOrdersDao ordersDao;
    @Override
    public List<Orders> findAll(int page,int size) throws Exception {
        PageHelper.startPage(page,size);
        return ordersDao.findAll();
    }

    @Override
    public Orders findById(String id) throws Exception{
       return ordersDao.findById(id);

    }
}
