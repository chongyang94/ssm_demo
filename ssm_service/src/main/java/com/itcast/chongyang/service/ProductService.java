package com.itcast.chongyang.service;

import com.itcast.chongyang.domain.Product;

import java.util.List;

public interface ProductService {
    public List<Product> findAll() throws Exception;

    public void save(Product product) throws Exception;
}
