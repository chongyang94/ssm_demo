package com.itcast.chongyang.service;

import com.itcast.chongyang.domain.SysLog;

import java.util.List;

public interface SysLogService {

    void save(SysLog sysLog) throws Exception;

    List<SysLog> findAll() throws Exception;
}
