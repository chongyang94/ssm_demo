package com.itcast.chongyang.service.text;

import com.itcast.chongyang.dao.IOrdersDao;
import com.itcast.chongyang.dao.IProductDao;
import com.itcast.chongyang.domain.Orders;
import com.itcast.chongyang.domain.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/applicationContext-service.xml")
public class ItemsServiceTest {

    @Autowired
    private IProductDao iProductDao;

    @Autowired
    private IOrdersDao iOrdersDao;

    @Test
    public void testFindAll() throws Exception {
        List<Product> products = iProductDao.findAll();
        System.out.println(products);
    }

    @Test
    public void testOrdersFindAll() throws Exception {
        List<Orders> all = iOrdersDao.findAll();
        System.out.println(all);
    }
}
