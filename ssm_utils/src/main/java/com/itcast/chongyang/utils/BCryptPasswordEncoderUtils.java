package com.itcast.chongyang.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BCryptPasswordEncoderUtils {

    private static BCryptPasswordEncoder bCryptPasswordEncoder =new BCryptPasswordEncoder();

    public static String encode(String password){
        String encode = bCryptPasswordEncoder.encode(password);
        return encode;
    }

    public static void main(String[] args) {
        String encode = encode("jerry");
        System.out.println(encode);
    }
}
